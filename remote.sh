#!/bin/sh
if [ "$#" -ne 1 ]; then
  echo "usage: ./remote.sh start|stop" >&2
  exit 1
fi

case $1 in 
"start")
    git init --bare -b develop ../git-sandbox-remote
    git remote add origin ../git-sandbox-remote
    git push -u origin develop
    ;;

"stop") 
    git remote remove origin
    rm -rf ../git-sandbox-remote
    ;;

*)
    echo "usage: remote.sh start|stop" >&2
    exit 1
    ;;
esac