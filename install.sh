#!/bin/sh
# remove origin remote if it exists
origin_exists=$((git ls-remote --exit-code origin 1>/dev/null) 2>&1)
if [ "$origin_exists" = "" ]; then
    git remote remove origin
fi
# create aliases for custom commands
git config alias.sbclear '!sh .commands/sbclear.sh'
git config alias.sbcommit '!sh .commands/sbcommit.sh'