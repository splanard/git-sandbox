# git-sandbox

This project is a useful sandbox to practice your Git skills. A bit like "Visualizing Git", but based on the real Git engine.

## Prerequisites

To use this sandbox, you need:
* a shell, such as "Git Bash" for example
* a way to visualize your Git graph (GUI)
    * `gitk` is a tool often installed with Git. `gitk --all &` will display a basic but useful graph.
    * For those using VSCode, I strongly recommend the "Git Graph" plugin.
    * See https://git-scm.com/downloads/guis for a list of standalone GUIs

## Installation

* Clone this repository
* Execute `./install.sh`

## Usage

### Warning!

> Whatever you do, do not move or delete the tag `init`! This would prevent some commands from working.

### Commit

Instead of using `commit` to create a commit, you can use `sbcommit`:

```shell
$ git sbcommit "my message"
```

This command uses the option `--allow-empty` of the `commit` command to generate an empty commit (no file modification required).

It's a short way to generate a commit to practice commits/branches manipulation.

### Reinitialize

At a time, if you want to reinitialize this repository to its initial blank state, use `git sbclear`

### Remote

If you want to simulate a remote repository:

```shell
$ ./remote.sh start
```

This will initialize another Git repository `git-sandbox-remote`, at the same location as the first one.

By default, the branch `develop` will be pushed in its current state.

You can then interact with the remote by pushing/pulling.

When you don't need the remote, use:

```shell
$ ./remote.sh stop
```