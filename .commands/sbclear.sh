#!/bin/sh
# reset develop branch
git show-ref --verify --quiet refs/heads/develop
if [ $? = 0 ]
then
    git checkout -f develop
    git reset --hard init
else
    git branch develop init
    git checkout develop
fi
# remove other branches than "develop"
git branch | grep -v "develop" | xargs git branch -D 2>/dev/null
# remove other tags than "init"
git tag | grep -v "init" | xargs git tag -d