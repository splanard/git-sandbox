#!/bin/sh
if [ "$#" -ne 1 ]; then
  echo "usage: git sbcommit <message>" >&2
  exit 1
fi
git commit --allow-empty -m \"$1\"